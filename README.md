# videoplayer

Javascript Challenge - Video for JobsityI

Install instructions:

- Clone the repository
- Move to the new folder and run `npm install` or `yarn` to install dependencies
- Run `npm start` or `yarn start` to start the server
- Navigate to http://localhost:3000 if it doesn't start automatically
  
  
## Features

- Video Player with custom controls
- Ability to create, edit and delete clips
- Persistent clips using Local Storage
- Material Design Interface using the Material-UI library

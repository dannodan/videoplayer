import React, { Component } from 'react'
import MoreVert from '@material-ui/icons/MoreVert'
import IconButton from '@material-ui/core/IconButton';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import { connect } from 'react-redux';
import { openModal, deleteClip } from '../actions/index';

const mapDispatchToProps = dispatch => {
  return {
    openModal: (edit, clip) => dispatch(openModal(edit, clip)),
    deleteClip: id => dispatch(deleteClip(id))
  }
}

class ConnectedThumbMenu extends Component {

  state = {
    anchorEl: null,
  };

  handleClick = event => {
    this.setState({ anchorEl: event.currentTarget });
  };

  handleEdit = () => {
    this.props.openModal(this.props.index, this.props.clip);
    this.handleClose();
  }

  handleDelete = () => {
    const index = this.props.index;
    this.props.deleteClip(index);
    this.handleClose();
  }

  handleClose = () => {
    this.setState({ anchorEl: null });
  };

  render() {
    const { anchorEl } = this.state;
    return (
      <div>
        <IconButton
          aria-owns={anchorEl ? 'simple-menu' : null}
          aria-haspopup="true"
          onClick={this.handleClick}>
          <MoreVert/>
        </IconButton>
        <Menu
          id="simple-menu"
          anchorEl={anchorEl}
          open={Boolean(anchorEl)}
          onClose={this.handleClose}
        >
          <MenuItem onClick={this.handleEdit}>Edit</MenuItem>
          <MenuItem onClick={this.handleDelete}>Delete</MenuItem>
        </Menu>
      </div>
    )
  }
}

const ThumbMenu = connect(null, mapDispatchToProps)(ConnectedThumbMenu);
export default ThumbMenu;
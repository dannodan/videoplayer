import React, { Component } from 'react'
import ThumbMenu from './thumb-menu';
import ListItem from '@material-ui/core/ListItem';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import ListItemText from '@material-ui/core/ListItemText';

export default class Thumbnail extends Component {
  handleOpen = () => {
    this.props.handleOpen();
  }
  handleDelete = () => {
    this.props.handleDelete();
  }

  render() {

    console.log(this.props);

    let data = this.props.data;
    let menu = (!this.props.original) ? (<ThumbMenu index={this.props.index} clip={this.props.data}/>) : '';
    let click = this.props.onClick;
    let selected = (this.props.selected) ? {'background':'#ddd'} : {};
    return (
      <ListItem
        key={data.name}
        role={undefined}
        dense
        button
        onClick={click}
        style={selected}
      >
        <ListItemText primary={data.name} />
        <ListItemSecondaryAction>
          {menu}
        </ListItemSecondaryAction>
      </ListItem>
    
    )
  }
}

import React, { Component } from 'react';
import IconButton from '@material-ui/core/IconButton';
import PlayArrow from '@material-ui/icons/PlayArrow';
import Pause from '@material-ui/icons/Pause';
import Stop from '@material-ui/icons/Stop';
import Replay from '@material-ui/icons/Replay';
import { Container, Row, Col } from 'reactstrap';
import { connect } from 'react-redux';
import { playClip, pauseClip, stopClip, updatePlayer } from '../actions/index';

const mapDispatchToProps = dispatch => {
    return {
        playClip: () => dispatch(playClip()),
        pauseClip: () => dispatch(pauseClip()),
        stopClip: () => dispatch(stopClip()),
        updatePlayer: (paused, ended) => dispatch(updatePlayer(paused, ended))
    }
}

const mapStateToProps = state => {
    return {
        paused: state.playerState.paused,
        ended: state.playerState.ended,
        clip: state.selectedClip
    }
}

class ConnectedPlayer extends Component {
    componentDidMount () {
        let references = this.refs;
        let video = references.videoplayer;

        this.timerID = setInterval(
            () => this.tick(video),
            1000
        );

        references.play.addEventListener('click', () => {
            if (this.props.ended) {
                video.load();
                this.props.stopClip();
            }
            if (video.paused) {
                video.play();
                this.props.playClip();
            }
        });
        references.stop.addEventListener('click', () => {
            if (!this.props.ended) {
                video.load();
                this.props.stopClip();
            }
        });
        references.replay.addEventListener('click', () => {
            video.load();
            video.play();
            this.props.playClip();
        });
        references.pause.addEventListener('click', () => {
            if (!video.paused) {
                video.pause();
                this.props.pauseClip();
            }
        });
    }

    componentWillUnmount() {
        clearInterval(this.timerID);
    }

    componentWillReceiveProps(newProps) {
        if (JSON.stringify(this.props.clip) !== JSON.stringify(newProps.clip)) {
          this.setClip(newProps.clip);
        }
    }
    setClip(clip) {
        let references = this.refs;
        let video, source, clipURL;
        if (!!references.videoplayer) {
            video = references.videoplayer;
            source = video.getElementsByTagName('source')[0];
            clipURL = source.getAttribute('data-original') + '#t=' + clip.startTime + ',' + clip.endTime;
            source.setAttribute('src', clipURL);
            video.load();
        }
        
    }
    tick(video) {
        this.props.updatePlayer(video.paused, (video.currentTime >= this.props.clip.endTime))
      }
    render () {
        return (
            <Container>
                <Row>
                    <Col xs="12">
                        <video ref="videoplayer" width="100%" style={{'maxHeight':'400px'}}>
                            <source
                                src="https://download.blender.org/durian/trailer/sintel_trailer-480p.mp4"
                                data-original="https://download.blender.org/durian/trailer/sintel_trailer-480p.mp4"/>
                        </video>
                    </Col>
                </Row>
                <Row>
                    <Col xs={{size: 2, offset: 2}}>
                        <div ref="play">
                        <IconButton disabled={!this.props.paused}>
                            <PlayArrow/>
                        </IconButton>
                        </div>
                    </Col>
                    <Col xs="2">
                        <div ref="pause">
                            <IconButton disabled={this.props.paused || this.props.ended}>
                                <Pause/>
                            </IconButton>
                        </div>
                    </Col>
                    <Col xs="2">
                        <div ref="replay">
                            <IconButton>
                                <Replay/>
                            </IconButton>
                        </div>
                    </Col>
                    <Col xs="2">
                        <div ref="stop">
                            <IconButton disabled={this.props.ended}>
                                <Stop/>
                            </IconButton>
                        </div>
                    </Col>
                </Row>
            </Container>
        );

    }

}

const Player = connect(mapStateToProps, mapDispatchToProps)(ConnectedPlayer);

export default Player
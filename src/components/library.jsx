import React from "react";
import { connect } from "react-redux";
import Thumbnail from './thumbnail';
import List from '@material-ui/core/List';
import Button from '@material-ui/core/Button';
import AddIcon from '@material-ui/icons/Add';
import Modal from './modal';
import store from '../store/index';
import { openModal, selectClip, loadLocalStorage } from '../actions/index';

const mapStateToProps = state => {
  return { 
        entries: state.entries,
        open: state.modalState.open,
        edit: state.modalState.edit,
        loadedClip: state.modalState.loadedClip
    };
};

const mapDispatchToProps = dispatch => {
    return {
        openModal: clip => dispatch(openModal(clip)),
        selectClip: clip => dispatch(selectClip(clip)),
        loadLocalStorage: () => dispatch(loadLocalStorage())
    }
}

class ConnectedLibrary extends React.Component {
    handleOpen = (id) => {
        let entries = this.props.entries;
        let index = (!!id) ? id : entries.length+1;
        let edit = (!!id) ? true : false;
        this.props.openModal(false);
        console.log(store.getState());
        this.setState({open: true, editId: index, edit: edit});
    }
    handleSelect = (id, clip) => {
        this.props.selectClip(clip);
    }
    componentDidMount() {
        this.props.loadLocalStorage();
    }
    render() {
        let entries = this.props.entries;
        return (
            <div>
                <List>
                    {entries.map((entry, index) => {
                            let original = (!!entry.original) ? true : false;
                            //let selected = (this.state.selectedId === index) ? true : false;
                            return (
                                <Thumbnail
                                    className='fancy'
                                    key={index}
                                    index={index}
                                    data={entry}
                                    original={original}
                                    selected={false}
                                    onClick={() => this.handleSelect(index, entry)}/>
                            );
                        })
                    }
                </List>
                <Button variant="extendedFab" color="secondary" aria-label="Add" onClick={() => this.handleOpen(false)}>
                    <AddIcon />
                    Create Clip
                </Button>
                <Modal open={this.props.open} edit={this.props.edit} clip={this.props.loadedClip}/>
            </div>
        )
    }
};

const Library = connect(mapStateToProps, mapDispatchToProps)(ConnectedLibrary);
export default Library;
import React from 'react';
import TextField from '@material-ui/core/TextField';
import {Container, Row, Col} from 'reactstrap';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import { connect } from 'react-redux';
import { addClip, editClip, closeModal } from '../actions/index';

const mapDispatchToProps = dispatch => {
    return {
      addClip: clip => dispatch(addClip(clip)),
      editClip: (index, clip) => dispatch(editClip(index, clip)),
      closeModal: () => dispatch(closeModal())
    };
  };

class ConnectedModal extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            title: "",
            name: "",
            startTime: "",
            endTime: "",
            index: undefined,
            error: {
                status: false,
                content: ""
            }
        }
    }
    handleClose = () => {
        this.setState({name:'', startTime:'', endTime:'', error: {status: false, content: ""}});
        this.props.closeModal();
    }
    handleCreate = () => {
        let clip = {
            name: this.state.name,
            startTime: this.state.startTime,
            endTime: this.state.endTime
        }
        let error = this.validate();
        if (!error.status){
            this.props.addClip(clip);
            this.setState({name:'', startTime:'', endTime:'', error: error});
            this.handleClose();
        } else {
            this.setState({error: error});
        }
    }
    handleEdit = () => {
        let clip = {
            name: this.state.name,
            startTime: this.state.startTime,
            endTime: this.state.endTime
        }
        let index = this.props.edit;
        let error = this.validate();

        console.log(this.props);
        console.log(clip);

        if (!error.status){
            this.props.editClip(index, clip);
            this.setState({name:'', startTime:'', endTime:'', error: error});
            this.handleClose();
        } else {
            this.setState({error: error});
        }
    }
    handleChange = property => event => {
        this.setState({
            [property]: event.target.value,
        });
        
    };

    validate() {
        if (this.state.startTime < 0 || this.state.startTime > 52) {
            return {status: true, content: "Invalid Start Time"}
        }
        if (this.state.endTime < 0 || this.state.endTime > 52) {
            return {status: true, content: "Invalid End Time"}
        }
        if (this.state.startTime > this.state.endTime) {
            return {status: true, content: "Start Time must be lower than End Time"}
        }
        return {status: false, content: ""}
    }

    componentWillReceiveProps(newProps) {

        let edit = newProps.edit;
        let clip = newProps.clip;

        let index = (!!newProps.edit) ? newProps.index : undefined;

        this.setState({title: (!edit) ? "Create new Clip" : "Edit Clip", index: index});

        if (edit && !!clip) {
            this.setState({name:clip.name, startTime: clip.startTime, endTime: clip.endTime});
        }

    }
    render() {

        let edit = this.props.edit;
        let error = this.state.error;

        let finishButton = (edit) ? 
            <Button variant="contained" onClick={this.handleEdit} color="primary">Save</Button> :
            <Button variant="contained" onClick={this.handleCreate} color="primary">Create</Button>

        let errorMessage = (error) ? error.content : ""

        return (
            <Dialog
                open={this.props.open}
                onClose={this.handleClose}
                aria-labelledby="form-dialog-title"
                >
                <DialogTitle>
                    {this.state.title}
                </DialogTitle>
                <DialogContent>
                    <form noValidate autoComplete="off">
                        <Container>
                            <Row>
                                <Col xs="12">
                                    <TextField
                                    id="name"
                                    label="Name"
                                    value={this.state.name}
                                    onChange={this.handleChange('name')}
                                    fullWidth
                                    margin="normal"
                                    />
                                </Col>
                            </Row>
                            <Row>
                                <Col xs="12" md="6">
                                    <TextField
                                    id="start"
                                    label="Start Time"
                                    value={this.state.startTime}
                                    type="number"
                                    onChange={this.handleChange('startTime')}
                                    fullWidth
                                    margin="normal"
                                    />
                                </Col>
                                <Col xs="12" md="6">
                                    <TextField
                                    id="end"
                                    label="End Time"
                                    value={this.state.endTime}
                                    type="number"
                                    onChange={this.handleChange('endTime')}
                                    fullWidth
                                    margin="normal"
                                    />
                                </Col>
                            </Row>
                            <Row>
                                <Col xs="12" style={{textAlign: 'center', color:"red"}}>
                                    {errorMessage}
                                </Col>
                            </Row>
                        </Container>
                    </form>
                </DialogContent>
                <DialogActions>
                    <Button variant="contained" onClick={this.handleClose} color="primary">
                        Cancel
                    </Button>
                    {finishButton}
                </DialogActions>
            </Dialog>
        )
    }
}

const Modal = connect(null, mapDispatchToProps)(ConnectedModal);
export default Modal;
import {
  ADD_CLIP,
  PLAY_CLIP,
  PAUSE_CLIP,
  STOP_CLIP,
  EDIT_CLIP,
  DELETE_CLIP,
  SELECT_CLIP,
  OPEN_MODAL,
  CLOSE_MODAL,
  LOAD_LOCAL_STORAGE, 
  UPDATE_PLAYER} from "../constants/action-types";

const initialState = {
    entries: [
      {
        name: "Original Video",
        startTime: 0,
        endTime: 52,
        original: true
      }
    ],
    modalState: {
      open: false,
      edit: false,
      loadedClip: {}
    },
    selectedClip: {
      name: "Original Video",
      startTime: 0,
      endTime: 52,
      original: true
    },
    playerState: {
      paused: true,
      ended: true,
    }
  };

  function updateObject(oldObject, newValues) {
    return Object.assign({}, oldObject, newValues);
  }

  function updateItemInArray(array, itemId, updateItemCallback) {
    const updatedItems = array.map((item, index) => {
      if(index !== itemId) {
          return item;
      }
      const updatedItem = updateItemCallback(item);
      return updatedItem;
    });

    return updatedItems;
  }

  function addClip(state, action) {
    const payload = action.payload;
    const newEntries = state.entries.concat({
      name: payload.name,
      startTime: payload.startTime,
      endTime: payload.endTime,
      original: false
    })

    window.localStorage.setItem('player-entries', JSON.stringify(newEntries));
    return updateObject(state, {entries: newEntries});
  }

  function editClip(state, action) {
    const payload = action.payload;
    const newEntries = updateItemInArray(state.entries, payload.id, clip => {
      return updateObject(clip, {name : payload.clip.name, startTime: payload.clip.startTime, endTime: payload.clip.endTime});
    });
    
    window.localStorage.setItem('player-entries', JSON.stringify(newEntries));
    return updateObject(state, {entries : newEntries});
  }

  function deleteClip(state, action) {
    const payload = action.payload;
    const newEntries = Object.assign([], state.entries);
    newEntries.splice(payload, 1);
    window.localStorage.setItem('player-entries', JSON.stringify(newEntries));
    return updateObject(state, {entries: newEntries});
  }

  function selectClip(state, action) {
    const payload = action.payload;
    const newClip = updateObject(state.selectedClip, {name: payload.name, startTime: payload.startTime, endTime: payload.endTime, original: payload.original});
    return updateObject(state, {selectedClip: newClip});
  }

  function openModal(state, action) {
    const payload = action.payload;
    const modalState = updateObject(state.modalOpen, { open: true, edit: payload.edit, loadedClip: payload.clip });
    return updateObject(state, {modalState: modalState});
  }

  function closeModal(state, action) {
    const modalState = updateObject(state.modalOpen, { open: false });
    return updateObject(state, {modalState: modalState});
  }

  function playClip(state, action) {
    const newPlayerState = updateObject(state.playerState, {paused: false, ended: false});
    return updateObject(state, {playerState: newPlayerState});
  }

  function pauseClip(state, action) {
    const newPlayerState = updateObject(state.playerState, {paused: true, ended: false});
    return updateObject(state, {playerState: newPlayerState});
  }

  function stopClip(state, action) {
    const newPlayerState = updateObject(state.playerState, {paused: true, ended: true});
    return updateObject(state, {playerState: newPlayerState});
  }

  function loadLocalStorage(state, action) {
    let storedEntries = JSON.parse(window.localStorage.getItem('player-entries'));
    const newEntries = (!!storedEntries) ? storedEntries : state.entries;
    return updateObject(state, {entries: newEntries});
  }

  function updatePlayer(state, action) {
    const payload = action.payload;
    const newPlayerState = updateObject(state.playerState, {paused: payload.paused, ended: payload.ended});
    return updateObject(state, {playerState: newPlayerState});
  }

  const rootReducer = (state = initialState, action) => {
    switch (action.type) {
      case ADD_CLIP: return addClip(state, action);
      case EDIT_CLIP: return editClip(state, action);
      case DELETE_CLIP: return deleteClip(state, action);
      case SELECT_CLIP: return selectClip(state, action);
      case OPEN_MODAL: return openModal(state, action);
      case CLOSE_MODAL: return closeModal(state, action);
      case PLAY_CLIP: return playClip(state, action);
      case PAUSE_CLIP: return pauseClip(state, action);
      case STOP_CLIP: return stopClip(state, action);
      case LOAD_LOCAL_STORAGE: return loadLocalStorage(state, action);
      case UPDATE_PLAYER: return updatePlayer(state, action);
      default:
        return state;
    }
  };

  export default rootReducer;
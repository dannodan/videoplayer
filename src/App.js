import React, { Component } from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import './App.css';
import Player from './components/player';
import Library from './components/library';
import { Container, Row, Col } from 'reactstrap';

class App extends Component {

  constructor(props) {
    super(props);
    this.state = {
      selected: {
        name: "Original Video",
        start: 0,
        end: 52
      }
    };
    this.selectClip = this.selectClip.bind(this);
  }

  selectClip(clip) {
    this.setState({selected: clip})
  }

  render() {
    return (
      <div className="App">
        <AppBar position='static'>
          <Toolbar>
            <Typography variant="title" color="inherit">Video Player</Typography>
          </Toolbar>
        </AppBar>
        <Container>
          <Row>
            <Col xs="12" lg="9">
              <Player clip={this.state.selected}/>
            </Col>
            <Col xs="12" lg="3">
              <Library handleSelect={this.selectClip}/>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

export default App;
